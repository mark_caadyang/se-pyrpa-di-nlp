# model for softben vouchers
# must sync with the columns on the input file
# can also put mapping methods here during init_process

class SoftBenVoucherModel:
    
    def __init__(self, first_name, middle_name, last_name, employee_id):
        # TODO: Change Fields
        self.first_name = first_name
        self.middle_name = middle_name
        self.last_name = last_name
        self.employee_id = employee_id

def map_softben_vouchers(df):
    if len(df) < 1:
        return None
    softben_vouchers = []
    for index, row in df.iterrows():
        model = SoftBenVoucherModel(
            first_name=row["first_name"],
            middle_name=row["middle_name"],
            last_name=row["last_name"],
            employee_id=row["employee_id"]
        )
        softben_vouchers.append(model)
    
    return softben_vouchers