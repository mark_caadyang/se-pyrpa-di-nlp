"""
TEST FILE

TODO: DELETE
"""

from behave import given, when, then
from services.test_file import Calculator, OpenGoogle

# SCENARIO 1
@given(u"I input {number_one} and {number_two} in the calculator")
def step_input_number(context, number_one, number_two):
    context.calculator = Calculator()
    context.calculator.input_numbers(int(number_one), int(number_two))

@when(u"I add")
def step_add(context):
    context.calculator.add()

@then(u"I verify that the result is {result}")
def step_verify_result(context, result):
    res = int(result)
    assert context.calculator.get_res() == res

# SCENARIO 2
@given(u"user is on google home")
def step_open_google(context):
    context.google_obj = OpenGoogle(context.browser)
    context.google_obj.open_google()

@then(u"page title should be {expected_title}")
def step_page_title(context, expected_title):
    title = context.google_obj.get_page_title()
    assert title == expected_title

@when(u"user searches for {search_text}")
def step_google_search(context, search_text):
    context.google_obj.google_search(search_text)

@then(u"the search results page is displayed")
def step_assert_search_results(context):
    assert "Google Search" in context.browser.title
    assert "Results for" in context.browser.page_source