from selenium import webdriver
from behave import fixture, use_fixture

from services.test_file import OpenGoogle

@fixture
def browser_chrome(context):
    context.browser = webdriver.Chrome()
    context.google_obj = OpenGoogle(context.browser)
    # yield = executes the steps
    yield context.browser
    # teardown/cleanup
    context.browser.quit()

def before_tag(context, tag):
    if tag == "web":
        use_fixture(browser_chrome, context)