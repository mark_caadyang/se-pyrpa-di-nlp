Feature: Sample BDD Test Scenario
    Scenario: Addition of two integers
        Given I input 5 and 6 in the calculator
        When I add
        Then I verify that the result is 11
    
    @web
    Scenario: Opening google chrome
        Given user is on google home
        Then page title should be Google
    
    @web
    Scenario: Opening google chrome and search globe telecom
        Given user is on google home
        When user searches for globe telecom
        Then the search results page is displayed
