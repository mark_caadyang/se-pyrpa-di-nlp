import logging
from datetime import datetime
import inspect
import sys

import pytz
from dotenv import find_dotenv, dotenv_values


def convert_to_rfc_datetime(year=1900, month=1, day=1, hour=0, minute=0):
    dt = datetime(year, month, day, hour, minute, 0).isoformat() + 'Z'
    return dt


def get_function_name(depth=1):
    # get the frame object of the function
    frame = inspect.currentframe()
    return f"{sys._getframe(depth).f_code.co_name}()"


def get_datetime_formatted():
    return datetime.now(tz=pytz.timezone('Asia/Manila')).strftime('%Y%m%d-%H%M%S')


def get_config(env_filename=None):
    """
        Throw warning if Asset is empty

        KeyError: check if your key is in .env.dev or .env.prod
    """
    if env_filename is None:
        env_filename = find_dotenv()

    # set all env values to config, check for empty values, and throw warning, if empty value is found
    config = dotenv_values(find_dotenv(env_filename))

    log_config = logging.getLogger(__name__)
    logging.basicConfig(level=logging.INFO,
                        format='%(message)s')
    log_config.info(f"Loading config from file: [{env_filename}]")

    for key, val in config.items():
        if (val is None or val == '' or
                config.get(key) is None or config.get(key) == ''):
            logging.warning(f"Initializing Project Config: [{key}], returned empty or null: [{val}]")

    return config
