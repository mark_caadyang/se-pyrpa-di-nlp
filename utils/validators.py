import os, json
import pandas as pd 

# from util.logger import logger

class InputValidator():

    def __init__(self):
        self.proj_dir = os.getcwd()
        self.add_file = None
        self.edit_file = None

    def validate_filename(self, filename):
        if filename.startswith("add_"):
            self.add_file = filename
            return True
        elif filename.startswith("edit_"):
            self.edit_file = filename
            return True
        else:
            return False
    
    def validate_folder_contents(self, folder_path):
        if not os.path.exists(folder_path) or not os.path.isdir(folder_path):
            # logger.info(f"The specified folder '{folder_path}' does not exist.")
            return False
        
        files = os.listdir(folder_path)

        if not files:
            # logger.info(f"The folder '{folder_path}' is empty.")
            return False
        
        for file in files:
            if self.validate_filename(file):
                # logger.info(f"Valid file: {file}")
                print("pass")
            else:
                # logger.info(f"Invalid file: {file}")
                return False
            
        return True, self.add_file, self.edit_file


class FileValidator():

    def __init__(self):
        pass

    def dataframe(self, file):
        df = pd.DataFrame(file["rows"])
        return df