# methods for soft ben vouchers transaction flow
# accessing elements only 

from services.workplan.init.webdriver import IsgWebDriver
from model.softben_vouchers_model import map_softben_vouchers

import pandas as pd

# selenium
from selenium.common import (ElementNotVisibleException,
                             ElementNotSelectableException,
                             NoSuchElementException,
                             TimeoutException)
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC

class SoftBenVouchers():

    def __init__(self, browser: IsgWebDriver, records):
        self.browser = browser
        self.records = records

    # must move this to initialization
    def map(self):
        self.rows = map_softben_vouchers(self.df)

    # this will perform the browser automation
    # loop the logic inside depending on how many rows/elements in the self.rows array
    def execute(self):
        print("starting execution...")

        #
        # some automation code here to go to the form/ui to input the model
        #

        if len(self.rows) > 0:
            for row in self.rows:
                print(row.first_name)
                print(row.middle_name)
                print(row.last_name)
                print(row.employee_id)


                #
                # some automation code here to locate the elements and send_keys() or click()
                # use find_element_until_visible
                # 

                # apply other business requirements
                # updating status, retries, error handling, breaks