from services.browser.sa_donation_vouchers import DonationVouchers
from services.browser.sa_softben_vouchers import SoftBenVouchers
from services.browser.sa_telco_vouchers import TelcoVouchers
from services.browser.sa_vouchers import Vouchers

from services.workplan.init.webdriver import IsgWebDriver

import pandas as pd

class EditWorkplan():

    def __init__(self, browser: IsgWebDriver):
        self.browser = browser

    def process(self, df, filename):
        # update logic
        if filename == "donation":
            steps_donation = DonationVouchers(self.browser, df)
        elif filename == "softben":
            steps_softben = SoftBenVouchers(self.browser, df)
        elif filename == "telco":
            steps_telco = TelcoVouchers(self.browser, df)
        elif filename == "voucher":
            steps_voucher = Vouchers(self.browser, df)
        else:
            # placeholder
            steps_donation = DonationVouchers(self.browser, df)