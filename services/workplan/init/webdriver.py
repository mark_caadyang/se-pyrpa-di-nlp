from __future__ import annotations

import traceback

from RPA.Browser.Selenium import Selenium, Locator, TimeoutType
from robotlibcore import DynamicCore
from robotlibcore import keyword

from selenium.common import (ElementNotVisibleException,
                             ElementNotSelectableException,
                             NoSuchElementException,
                             TimeoutException)
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support import expected_conditions as EC

from dotenv import load_dotenv, find_dotenv
from typing import Optional
import math
import os


import requests
from googleapiclient.errors import HttpError

from utils.helper import get_function_name
# from utils.logger import logger

load_dotenv(find_dotenv())

# os.environ['WDM_LOG_LEVEL'] = '0'
# os.environ['WDM_PRINT_FIRST_LINE'] = 'False'
# os.environ['WDM_LOCAL'] = '1'

# project_path = os.path.abspath(os.getcwd())
# driver_path = os.environ.get("WEB_DRIVER_PATH", f"{project_path}/drivers")
# auto_close_browser = os.environ.get("AUTO_CLOSE_BROWSER", False).lower() in ('true', '1', 't')
# browser_screen = os.getenv("BROWSER_SCREEN", "full").lower()
# GLOBAL_EXPLICIT_WAIT = float(os.getenv("GLOBAL_WAIT", 3))

browser_screen = "full"
GLOBAL_EXPLICIT_WAIT = 5


class IsgWebDriver(Selenium):

    def __init__(self):
        super().__init__(auto_close=False)
        self.actions = None

    def __create_driver(self, browser, alias, path, options=None, kwargs={}):
        driver_name = browser

        index = self.create_webdriver(driver_name=driver_name,
                                      options=options)  # Create an instance of WebDriver based on driver name(Firefox, Chrome, Ie, Edge, Safari)

        index_or_alias = alias or index
        return index_or_alias

    @keyword
    def open_to_chrome(self, url: str = "", path: Optional[str] = "", alias: Optional[str] = None,
                       headless: bool = False):
        options = None

        options = Options()
        options.add_experimental_option("detach", True)
        options.add_argument('--ignore-ssl-errors=yes')
        options.add_argument('--ignore-certificate-errors')

        # temporarily comment out
        if browser_screen in "full":
            options.add_argument("--start-maximized")

        if headless:
            options.headless = True

        # Initialize Webdriver
        browser_name = self.__create_driver("Chrome", alias, path, options)
        if url and browser_name:

            if browser_screen in "right":
                # get screen width and height
                screen_width = self.driver.execute_script("return window.screen.availWidth;")
                screen_height = self.driver.execute_script("return window.screen.availHeight;")

                # Get right half
                right_half_width = math.ceil(screen_width / 2)
                right_half_height = screen_height

                # set position
                self.driver.set_window_position(right_half_width, 0)
                self.driver.set_window_size(right_half_width, right_half_height)

            # launches a new browser and opens the given URL in your Webdriver
            self.driver.get(url)
            self.driver.implicitly_wait("5")
            # Initializes Action Chains
            self.actions = ActionChains(self.driver)

        return browser_name

   
    @keyword
    def check_landing(self, page):
        # TODO - provide logic, just assert the title
        if page == "Home":
            pass
   
    # @keyword
    # def open_to_firefox(self, url: str = "", path: str = "", alias: Optional[str] = None, headless: bool = False):
    #     options = None
    #
    #     if headless:
    #         options = FirefoxOptions()
    #         options.headless = True
    #         options.add_argument("--window-size=1920,1080")
    #         # options.add_argument("--start-maximized")
    #         options.add_argument('--ignore-ssl-errors=yes')
    #         options.add_argument('--ignore-certificate-errors')
    #         options.add_experimental_option("detach", True)
    #         # error when running with user profile
    #         # options.add_argument("user-data-dir=C:\\Users\\Yasmin\\AppData\\Local\\Google\\Chrome\\User Data")
    #         # options.add_argument("--profile-directory=Profile 1")
    #
    #     browser_name = self.__create_driver("Firefox", alias, path, options)
    #     if url and browser_name:
    #         self.driver.get(url)
    #     return browser_name
    # @keyword
    # def open_to_chromium(self, url: str = "", path: Optional[str] = "", alias: Optional[str] = None):
    #     browser_name = self.__create_driver("Chrome", alias, path, kwargs={'chrome_type': ChromeType.CHROMIUM})
    #     if url and browser_name:
    #         self.go_to(url)
    #     return browser_name
    #
    # @keyword
    # def open_to_ie(self, url: str = "", path: Optional[str] = "", alias: Optional[str] = None):
    #     browser_name = self.__create_driver("IE", alias, path)
    #     if url and browser_name:
    #         self.go_to(url)
    #     return browser_name
    #
    # @keyword
    # def open_to_edge(self, url: str = "", path: Optional[str] = "", alias: Optional[str] = None):
    #     browser_name = self.__create_driver("Edge", alias, path)
    #     if url and browser_name:
    #         self.go_to(url)
    #     return browser_name
    #
    # @keyword
    # def open_to_opera(self, url: str = "", path: Optional[str] = "", alias: Optional[str] = None):
    #     browser_name = self.__create_driver("Opera", alias, path)
    #     if url and browser_name:
    #         self.go_to(url)
    #     return browser_name

    def find_element_until_visible(self, by: str, element_id: str, wait=GLOBAL_EXPLICIT_WAIT, throw_error=False) -> (
            WebElement | None):
        """
        Find element with explicit wait.
        If throw_error is True: Will throw error if element is not found
        :param by: By.ID, By.XPATH etc. See https://selenium-python.readthedocs.io/locating-elements.html
        :param element_id: element ID of the target, must match by
        :param wait: seconds to wait before throwing error
        :param throw_error: boolean, default False. Return ValueError if true and element was not found within the wait time
        :return:
        """
        try:
            wait = WebDriverWait(self.driver, wait, poll_frequency=1,
                                 ignored_exceptions=[ElementNotVisibleException, ElementNotSelectableException])
            return wait.until(EC.presence_of_element_located((by, element_id)))

        except (NoSuchElementException, TimeoutException) as e:
            if throw_error:
                raise ValueError(f"{get_function_name()}: unable to locate by: [{by}], element: {element_id}, wait: [{wait}]")

            # logger.warning(f"{get_function_name()}: unable to locate by: [{by}], element: {element_id}, wait: [{wait}]")
            return None

    # def find_element_until_clickable(self, by, value, wait: float = GLOBAL_EXPLICIT_WAIT, throw_error=False):
    #     try:
    #         element = WebDriverWait(self.driver, wait).until(
    #             EC.element_to_be_clickable((by, value)))

    #         return element
    #     except:
    #         if throw_error:
    #             raise ValueError(f"{get_function_name()}: unable to locate by: [{by}], element: {value}, wait: [{wait}]")

    #         logger.warning(f"{get_function_name()}: unable to locate by: [{by}], element: {value}, wait: [{wait}]")

    # def click_when_clickable(self, by: By, element_id: Locator, timeout: TimeoutType = GLOBAL_EXPLICIT_WAIT,
    #                          throw_error=True) -> WebElement:
    #     """
    #     locate element. Click when visible
    #     :param element_id: str - element locator
    #     :param by: By.XPATH, By.CSS_SELECTOR etc. See https://selenium-python.readthedocs.io/locating-elements.html
    #     :param timeout: number of seconds to raise timeout
    #     :param throw_error:
    #     :return:
    #     """
    #     try:
    #         element = WebDriverWait(self.driver, timeout).until(
    #             EC.element_to_be_clickable((by, element_id)))

    #         element.click()

    #         return element

    #     except (NoSuchElementException, TimeoutException) as e:
    #         if throw_error:
    #             raise ValueError(f"{get_function_name()}: unable to locate by: [{by}], element: {element_id}, "
    #                              f"wait: [{timeout}]")

    #         logger.warning(f"Element not found. Element [{element_id}], by: [{by}]. Error encountered: {e}")

    # def scroll_element_to_view(self, by: By, value, wait: float = GLOBAL_EXPLICIT_WAIT,
    #                            clickable=True) -> WebElement:
    #     """
    #     Similar to self.web_driver.find_element, but it scrolls to the element to make it visible
    #     Also waits for the element to be clickable
    #     For elements that are already loaded but not yet in view. Otherwise, interacting to the element may fail
    #     :return: the element if found
    #     """
    #     try:
    #         if clickable:
    #             # Wait up to x seconds. Respond as soon as the element is visible
    #             element = WebDriverWait(self.driver, wait).until(
    #                 EC.element_to_be_clickable((by, value)))

    #         else:
    #             element = self.driver.find_element(by=by, value=value)

    #         # element = self.web_driver.find_element(by=by, value=value)
    #         self.actions.move_to_element(element).perform()

    #         return element

    #     except Exception:
    #         try:
    #             element = self.driver.find_element(by=by, value=value)
    #             return element
    #         except Exception:
    #             logger.warning(f"Failed to scroll element to view. Element [{value}], by: [{by}].")


    # def wait_for_one(self, elements: dict[str, list[str | str]]):
    #     """
    #     Checks dictionary for elements. Returns key of the first element to be found in the dictionary
    #     :param elements: dict: dictionary of elements you would like to find. ex. elements = {"value": [by, locator]}
    #     :param by: str: first value, By.ID, By.XPATH etc. See https://selenium-python.readthedocs.io/locating-elements.html.
    #     :param locator: str: second value, locator of the element. Ex. "my_class"
    #     :return: If found, key of the element found, and the element object itself
    #     """
    #     for key, value in elements.items():
    #         try:
    #             element = self.driver.find_element(value[0], value=value[1])
    #         except NoSuchElementException:
    #             pass
    #         else:
    #             return key, element

    #     return None, None


class IsgWebDriverLibrary(DynamicCore):

    def __init__(self):
        libraries = [IsgWebDriver()]
        DynamicCore.__init__(self, libraries)
