# methods to login to platform
# inject browser object 

from services.workplan.init.webdriver import IsgWebDriver

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

class Login():

    def __init__(self, browser: IsgWebDriver):
        self.browser = browser

    def goto_platform(self, url, browser):
        if browser == "Chrome":
            self.browser.open_to_chrome(url)
            self.browser.check_landing("Home")
    
    def enter_credentials(self, creds):
        user_input = self.browser.driver.find_element(by=By.NAME, value="username")
        user_input.send_keys(creds["login_username"])

        password_input = self.browser.driver.find_element(by=By.NAME, value="password")
        password_input.send_keys(creds["login_password"])

        login_btn = self.browser.driver.find_element(by=By.CLASS_NAME, value="orangehrm-login-button")
        login_btn.click()

    ## Other methods here related to logging in

