"""
TEST FILE

TODO: DELETE
"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

class OpenGoogle:
    def __init__(self, browser):
        self.browser = browser
    
    def open_google(self):
        self.browser.get("https://www.google.com")
    
    def get_page_title(self):
        return self.browser.title

    def google_search(self, input):
        search_input = self.browser.find_element("name", "q")
        search_input.send_keys(input)
        search_input.send_keys(Keys.RETURN)
        self.browser.implicitly_wait(5)


class Calculator:
    def __init__(self):
        self.integer_one = 0
        self.integer_two = 0
        self.res = 0
    
    def get_res(self):
        return self.res

    def input_numbers(self, int_one, int_two):
        self.integer_one = int_one
        self.integer_two = int_two
    
    def add(self):
        self.res = self.integer_one + self.integer_two
    

