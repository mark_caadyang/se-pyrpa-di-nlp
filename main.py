import json, os

# import initializations, utils, etc
from utils.validators import InputValidator, FileValidator
from services.workplan.init.webdriver import IsgWebDriver
from services.workplan.init.login_nlp import Login
from services.workplan.init.init_process import InitProcess

# import workplans
from services.workplan.add_workplan import AddWorkplan
from services.workplan.edit_workplan import EditWorkplan


def load_json(file_path="config.json"):
    try:
        with open(file_path, "r") as f:
            config_data = json.load(f)
            return config_data
    except FileNotFoundError:
        print(f"Error: The file '{file_path}' was not found.")
        return {}
    except json.JSONDecodeError as e:
        print(f"Error decoding '{file_path}': {e}")
        return {}

def main(config):

    """
    FLOW
    1   - Init: Validate GDrive, auth, etc.
    2   - Init: Get input files -> move to processing folder
    3   - Login: Access NLP Platform
    4   - Workplan: Add
    4.1 - Execute: Process files (donation, softben, telco, voucher)
    5   - Workplan: Edit
    5.1 - Execute: Process files (donation, softben, telco, voucher)
    6   - Cleanup: Logout/close browser/move files to output folder
    """

    # Placeholder logic for input and file validation
    # process the inputs here as well, map into models
    # actual logic must integrate Google api
    # init = InitProcess()

    input_validator = InputValidator()
    inputs = os.getcwd() + config["other_configs"]["inputs_path"]

    isValid, add_file, edit_file = input_validator.validate_folder_contents(inputs)

    file_validator = FileValidator()
    add_file_df = file_validator.dataframe(load_json(inputs + add_file)) if add_file is not None else None
    edit_file_df = file_validator.dataframe(load_json(inputs + edit_file)) if edit_file is not None else None

    if isValid:
        print("Input files valid...")

        # Login
        nlp = Login(browser)
        nlp.goto_platform(config["orange_hrm"]["login_url"], "Chrome")
        nlp.enter_credentials(config["orange_hrm"])

        # Start Workplan
        # refactor logic here based on the actual input files
        # add
        if add_file_df is not None:
            add_wp = AddWorkplan(browser)
            # sample: Add donation vouchers
            add_wp.process(add_file_df, add_file)

        # edit
        if edit_file_df is not None:
            edit_wp = EditWorkplan(browser)
            pass

        # Cleanup & Logout
        browser.driver.close()

    else:
        print("Please recheck input files...")
    return 0


if __name__ == "__main__":
    config = load_json()
    browser = IsgWebDriver()
    main(config)